package com.velykyi.concurrency;

public class CustomerReadWriteLock{
    private Object readSync;
    private Object writeSync;

    public CustomerReadWriteLock() {
        this.readSync = new Object();
        this.writeSync = new Object();
    }

    public Object readLock() {
        return readSync;
    }

    public Object writeLock() {
        return writeSync;
    }

    public void setReadLock() throws InterruptedException {
        this.readLock().wait();
    }

    public void setReadUnlock() {
        this.readLock().notify();
    }

    public void setWriteLock() throws InterruptedException {
        this.writeLock().wait();
    }

    public void setWriteUnlock() {
        this.writeLock().notify();
    }
}

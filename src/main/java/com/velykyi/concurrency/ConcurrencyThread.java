package com.velykyi.concurrency;

import java.time.LocalTime;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class ConcurrencyThread {
    private final static ReentrantLock LOCK = new ReentrantLock();

    public void invoke1() {
        Thread thread1 = run();
        Thread thread2 = run();
        Thread thread3 = run();
        thread1.start();
        thread2.start();
        thread3.start();
        try{
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void invoke2() {
        Thread thread1 = runnable();
        Thread thread2 = runnable();
        Thread thread3 = runnable();
        thread1.start();
        thread2.start();
        thread3.start();
        try{
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Thread run() {
        return new Thread(() -> {
            try {
                if (LOCK.tryLock(5, TimeUnit.SECONDS)) {
                    try {
                        LOCK.lock();
                        System.out.println("Lock was locked by " + Thread.currentThread().getName() + " " + LocalTime.now());
                    } finally {
                        LOCK.unlock();
                    }
                } else {
                    System.out.println("Sorry, lock is locked. " + Thread.currentThread().getName() + " " + LocalTime.now());
                }
                System.out.println(Thread.currentThread().getName() + " finish " + LocalTime.now());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    private void work() {
        try {
            LOCK.lock();
            System.out.println("Lock was locked by " + Thread.currentThread().getName() + " " + LocalTime.now());
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void finish() {
        LOCK.unlock();
        System.out.println(Thread.currentThread().getName() + " finish " + LocalTime.now());
    }

    private Thread runnable() {
        return new Thread(() -> {
           work();
           finish();
        });
    }
}

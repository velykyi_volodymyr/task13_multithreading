package com.velykyi.concurrency;

import java.util.Scanner;
import java.util.concurrent.BlockingQueue;

public class BlockingQueueUser {
    private BlockingQueue<String> queue;
    private Scanner scann;

    public BlockingQueueUser(BlockingQueue<String> queue) {
        this.queue = queue;
        this.scann = new Scanner(System.in);
    }

    public Thread putTake() {
        return runThread1();
    }

    public Thread takePut() {
        return runThread2();
    }

    private Thread runThread1() {
        return new Thread(() -> {
            String message = "";
            System.out.println("User 1 in " + Thread.currentThread().getName() + " , enter your message:");
            out(message);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            in(message);
        });
    }

    private Thread runThread2() {
        return new Thread(() -> {
            String message = "";
            in(message);
            System.out.println("User 2 in " + Thread.currentThread().getName() + " , enter your message:");
            out(message);
        });
    }

    private void out(String message) {
        do {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print("OUT " + Thread.currentThread().getName() + ": ");
            message = scann.nextLine();
            try {
                queue.put(message);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (!message.toLowerCase().equals("q"));
    }

    private void in(String message) {
        do {
            try {
                message = queue.take();
                System.out.println("IN " + Thread.currentThread().getName() + ": " + message);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (!message.toLowerCase().equals("q"));
    }
}

package com.velykyi.concurrency;

import java.time.LocalTime;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class App {
    public static void main(String[] args) {
//        ConcurrencyThread concurrencyThread = new ConcurrencyThread();
//        concurrencyThread.invoke2();

        //-----------------------------------------------------------------------------------------------------------

//        BlockingQueue<String> queue = new ArrayBlockingQueue<>(1);
//        BlockingQueueUser user1 = new BlockingQueueUser(queue);
//        BlockingQueueUser user2 = new BlockingQueueUser(queue);
//        Thread thread1 = user1.putTake();
//        Thread thread2 = user2.takePut();
//        thread1.start();
//        thread2.start();
//        try {
//            thread1.join();
//            thread2.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        //-------------------------------------------------------------------------------------------------------------

        CustomerReadWriteLock rwl = new CustomerReadWriteLock();
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    rwl.setWriteLock();
                    System.out.println("Lock was locked by " + Thread.currentThread().getName() + " " + LocalTime.now());
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                rwl.setWriteUnlock();
                System.out.println(Thread.currentThread().getName() + " finish " + LocalTime.now());
            }
        };

        Thread t1 = new Thread(run);
        Thread t2 = new Thread(run);
        Thread t3 = new Thread(run);
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

package com.velykyi.threads.controller;

import com.velykyi.threads.model.Fibonacci;
import com.velykyi.threads.model.PingPong;
import com.velykyi.threads.model.Sleep;
import com.velykyi.threads.model.callable.FibonacciCallable;
import com.velykyi.threads.model.section.CriticalSection;
import com.velykyi.threads.model.section.CriticalSections;

import java.util.concurrent.ExecutionException;

public class Controller {
    public void printPingPong(){
        PingPong pp = new PingPong();
        pp.playRunnable();
    }

    public void getFiboRunnable() {
        Fibonacci fibo = new Fibonacci(10);
        fibo.startRunnable();
    }

    public void gatFiboSum() {
        FibonacciCallable fibonacciCallable1 = new FibonacciCallable(10);
        FibonacciCallable fibonacciCallable2 = new FibonacciCallable(20);
        FibonacciCallable fibonacciCallable3 = new FibonacciCallable(30);

        try{
            fibonacciCallable1.getSum();
            fibonacciCallable2.getSum();
            fibonacciCallable3.getSum();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void getSleepThread() {
        Sleep sleepThreads1 = new Sleep(5);
        Sleep sleepThreads2 = new Sleep(10);
        sleepThreads1.createSleepingThreads();
        sleepThreads2.createSleepingThreads();
    }

    public void invokeMethods() {
        CriticalSection criticalSection = new CriticalSection();
        criticalSection.startMethods();
        CriticalSections criticalSections = new CriticalSections();
        criticalSections.startMethods();
    }
}

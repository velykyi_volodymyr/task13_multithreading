package com.velykyi.threads.model.callable;

import com.velykyi.threads.model.Fibonacci;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciCallable implements Callable {
    private int[] sequence;

    public FibonacciCallable(int n) {
        Fibonacci fibo = new Fibonacci(n);
        this.sequence = fibo.createSequence();
    }

    @Override
    public Object call() throws Exception {
        int sum = 0;
        for (int item : this.sequence) {
            sum = sum + item;
        }
        return sum;
    }

    public void getSum() throws ExecutionException, InterruptedException {
        ExecutorService exe = Executors.newSingleThreadExecutor();
        System.out.println("Sum " + exe.submit(this::call).get());
        exe.shutdown();
    }
}

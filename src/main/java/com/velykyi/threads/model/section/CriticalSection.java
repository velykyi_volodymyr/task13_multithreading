package com.velykyi.threads.model.section;

import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

public class CriticalSection {
    private Object sync;

    public CriticalSection() {
        sync = new Object();
    }

    private Thread method1() {
        return new Thread(() -> {
            synchronized (sync) {
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Method 1 done." + LocalTime.now());
            }
        });
    }

    private Thread method2() {
        return new Thread(() -> {
            synchronized (sync) {
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Method 2 done." + LocalTime.now());
            }
        });
    }

    private Thread method3() {
        return new Thread(() -> {
            synchronized (sync) {
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Method 3 done." + LocalTime.now());
            }
        });
    }

    public void startMethods() {
        method1().start();
        method2().start();
        method3().start();
    }
}

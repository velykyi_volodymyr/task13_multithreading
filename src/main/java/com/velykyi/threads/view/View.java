package com.velykyi.threads.view;

import com.velykyi.threads.controller.Controller;

import java.util.*;

public class View {
    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    private Controller controller = new Controller();
    private static Scanner scann = new Scanner(System.in);
    private Locale locale;
    private ResourceBundle bundle;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("9", bundle.getString("9"));
        menu.put("q", bundle.getString("q"));
    }

    public View() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::getPingPong);
        methodMenu.put("2", this::getFibonacci);
        methodMenu.put("3", this::getFibonacciSum);
        methodMenu.put("4", this::getSleptThreads);
        methodMenu.put("5", this::getMethods);
        methodMenu.put("6", this::setEnglish);
        methodMenu.put("7", this::setUkrainian);
        methodMenu.put("8", this::setGerman);
        methodMenu.put("9", this::setGreek);
    }

    public void showMenu() {
        String keyMenu;
        do {
            System.out.println("\nMENU:");
            for (String str : menu.values()) {
                System.out.println(str);
            }
            System.out.println("Please, select menu point.");
            keyMenu = scann.nextLine().toUpperCase();
            try {
                methodMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.toUpperCase().equals("Q"));
    }

    private void getMethods() {
        controller.invokeMethods();
    }

    private void getSleptThreads() {
        controller.getSleepThread();
    }

    private void getFibonacciSum() {
        controller.gatFiboSum();
    }

    private void getFibonacci() {
        controller.getFiboRunnable();
    }

    private void getPingPong() {
        controller.printPingPong();
    }

    private void setEnglish(){
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void setUkrainian(){
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void setGerman(){
        locale = new Locale("de");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void setGreek(){
        locale = new Locale("el");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }
}

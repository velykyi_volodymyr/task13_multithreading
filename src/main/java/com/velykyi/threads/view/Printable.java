package com.velykyi.threads.view;

@FunctionalInterface
public interface Printable {
    void print();
}

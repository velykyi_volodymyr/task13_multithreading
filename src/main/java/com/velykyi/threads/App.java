package com.velykyi.threads;

import com.velykyi.threads.view.View;

public class App {


    public static void main(String[] args) {
        View view = new View();
        view.showMenu();

//        Object sync = new Object();
//        try {
//            PipedOutputStream out = new PipedOutputStream();
//            PipedInputStream in = new PipedInputStream(out);
//            User1 user1 = new User1(in, out, sync);
//            User2 user2 = new User2(in, out, sync);
//            user1.startThreads();
//            user2.startThreads();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }
}
